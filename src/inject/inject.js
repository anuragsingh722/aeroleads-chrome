/// Start ChangeObserver ///

var ChangeObserver = function()
{
  this.observer = new window.WebKitMutationObserver(function(mutations, observer) {
    var flag = true;
      // fired when a mutation occurs
      mutations.forEach(function(mutation) {
        var node = mutation.target;
        if($(node).hasClass("saved_data")){
          flag = false;
        }
      });   
      if(flag) { initOrReinit() };
    });
  this.resume();
};

ChangeObserver.prototype.resume = function()
{
  this.observer.observe($("#search").get(0), {
    subtree: true,
    attributes: true
  });
};

ChangeObserver.prototype.pause = function()
{
  this.observer.disconnect();
};

/// End ChangeObserver ///


/// FUNCTIONS DEFINITION ///

// MATCH URLS
function debugLog(message) {
  console.log(message);
}

function addModal(){
  var html = "<div class='al_modal'>";
  html += "<div class='close_al_modal close_modal'></div>";
  html += "<div class='content_al_modal'><h3><img src='https://aeroleads.com/assets/aeroleads-icon.png' class='modal_icon'><b>Uh Oh! You do not have enough credits. </b></h3>";
  html += "<div><a href='https://aeroleads.com/pricing'>Upgrade</a> your plan or mail us at <a href='mailto:sales@aeroleads.com'>sales@aeroleads.com</a></div>"
  html += "<div style='text-align:right'><a href='#' style='cursor:pointer;color: #555; display:inline-block; margin-top: 5px;' class='close_modal'>Close</a></div>";
  html += "</div>";
  html += "</div>";
  $("body").append(html);
}

function displayAPIerror(){
  var html = "<div class='api_error'>";
  html += "<div class='api_error_bg close_bg'></div>";
  html += "<div class='api_dialog'>"
  html += "<h1>AeroLeads API key missing!</h1>";
  html += "<div class='api_message'>To get your api key, register to aeroleads.com and goto settings page from dashboard.";
  html += "<p>Enter your API key in the chrome-extension on your chromes menu bar</p></div>";
  html += "<div class='close_bg close_bg_btn' style='text-align:center'>Close</div>"
  html += "</div></div>";
  $("body").append(html);
}

function initOrReinit() {
  debugLog("initOrReinit");
  if(!location.href.match('^https?://www\.google\.(co\.in|com|com\.au)/.*site(%3A|:)[^+ ]*linkedin\.com') ) { return; }
  if(!state) { return; }
  console.log("api" + api_key);
  if(!api_key || api_key=="") {
    displayAPIerror();
    return;
  }
  if(timeoutHandle) {
    window.clearTimeout(timeoutHandle);
  }
  timeoutHandle = window.setTimeout(findProfiles, 100);
}

function checkCredit(){
  var url = "http://localhost:3000/credit_available";
  var url = "https://aeroleads.com/credit_available";
  $.post(url, {api_key: api_key})
  .success(function(response){
    if(response == "true"){
      console.log("GOTIT");
    }
    else if(response=="false"){
      addModal();
      $(".credit_left").val("false");
    }
  })
}


function addTransferBtn(){
  $(".g").each(function(){
    var anchor = $(this).find("h3.r a");
    var link = $(anchor).attr("data-href");
    var query = $('title').text();

    var location = "NA";
    var current_position = "NA";
    var current_company = "NA";
    var details = $(this).find("div.s .f.slp");
    var details_str = $(this).find("div.s .f.slp");
    if(details.length){
      details_str = $(details_str).text();
      details = $(details).text();
      location = details.split("-")[0];
      details = details.split("-")[1];
      current_position = details.split(" at")[0];
      current_company = details.split(" at")[1];
      if(current_company == ""){
        current_company = details.split("@")[1];
      }
    }

    if(current_company == "undefined" || typeof current_company == "undefined"){
      current_company = "NA";
    }

    if(location == "undefined" || typeof location == "undefined"){
      location = "NA";
    }

    if(current_position == "undefined" || typeof current_position == "undefined"){
      current_position = "NA";
    }

    if(typeof link == "undefined"){
      link = $(anchor).attr("href");
    }
    var title = $(anchor).text();

    // TRIMMING
    title = title.trim();
    link = link.trim();
    query = query.trim();
    
    location = location.trim();
    current_company = current_company.trim();
    current_position = current_position.trim();

    var add_btn = "<div class='aeroleads_action'><input class='aeroleads_chk' type='checkbox' data-url='"+link+"' data-title='"+title+"' data-query='"+query+"' data-location='"+location+"' data-company='"+current_company+"' data-position='"+current_position+"' data-details_str='"+details_str+"'><span class='aeroleads_btn' data-url='"+link+"' data-title='"+title+"' data-query='"+query+"'  data-location='"+location+"' data-company='"+current_company+"' data-position='"+current_position+"' data-details_str='"+details_str+"'>Transfer</span></div>";
    $(this).prepend(add_btn);
  });
}

function addSelectedBox(){
  var html = "<div class='aeroleads_add_selected'>";
  html += "<input type='hidden' class='credit_left' value='true'>";
  html+="<header><div class='logo'><img src='https://aeroleads.com/assets/aeroleads-icon.png' class='img-responsive'></div>";
  html+="<div class='brand'>AeroLeads</div>";
  html+="</header>";
  html+="<div class='content'><p>Transfer Selected to AeroLeads</p><span class='add_selected'>Add Selected</span></div>";
  html+="</div>";
  $("body").append(html);
}


// FIND PROFILES
function findProfiles() {
  if(initializing) return;
  debugLog("findProfiles");
  initializing = true;
  change_observer.pause();

  addTransferBtn();
  addSelectedBox();
  checkCredit();


  initializing = false;
  change_observer.resume();

}


/// FUNCTIONS DEFINITION ENDS ///



/// VARIABLE DEFINITIONS ///
var initializing = false;
var timeoutHandle = null;
var change_observer = new ChangeObserver();

var api_key = "";
var state = "";

// GET API KEY 
chrome.storage.sync.get('aeroleads_settings',function(obj){
  if(obj.aeroleads_settings != null){
    $("#api_key").val(obj.aeroleads_settings.api_key);
  }else{
    $("#api_key").val("");
  }
  api_key =  obj.aeroleads_settings.api_key;
  // console.log(api_key);
});
// GET AEROLEADS STATE
chrome.storage.sync.get('aeroleads_state',function(obj){
  if(obj.aeroleads_state != null){
    state = obj.aeroleads_state.state;
  }else{
    state = false;
  }
  // console.log(state);
});

window.setTimeout(initOrReinit, 100);

$("body").on("click", ".aeroleads_btn", function(){
  var creds = $(".credit_left").val();
  if(creds == "false"){
    $(".al_modal").fadeIn();
  }
  else if(creds == "true"){
    var data = {};
    var selected = [];

    var that = $(this);

    data['url'] = $(this).attr("data-url");
    data['title'] = $(this).attr("data-title");
    data['query'] = $(this).attr("data-query");
    data['location'] = $(this).attr("data-location");
    data['current_company'] = $(this).attr("data-company");
    data['current_position'] = $(this).attr("data-position");
    data['details_str'] = $(this).attr("data-details_str");
    selected.push(data);
    data = JSON.stringify(selected);


    $(that).html("saving...");
    $(that).addClass("saved_data").removeClass("aeroleads_btn").html("Saved");
    var parent = $(that).parent();
    var input = $(parent).find("input");
    $(input).hide();


    var ajax_data = {
      api_key : api_key,
      data : data
    };

    var url = "http://localhost:3000/prospects";
    var url = "https://aeroleads.com/prospects";
    console.log(ajax_data);
    $.post(url, ajax_data)
    .success(function(response){
      $(that).addClass("saved_data").removeClass("aeroleads_btn").html("Saved");
      var parent = $(that).parent();
      var input = $(parent).find("input");
      $(input).hide();
    })
    .fail(function(response){
      $(that).addClass("saved_data").html("Error");
    })
    .always(function(response){
      $(that).addClass("saved_data").removeClass("aeroleads_btn").html("Saved");
      var parent = $(that).parent();
      var input = $(parent).find("input");
      $(input).hide();
    });
    console.log(selected);
  }
});

$("body").on("click",".add_selected", function(){
  var creds = $(".credit_left").val();
  if(creds == "false"){
    $(".al_modal").fadeIn();
  }
  else if(creds == "true"){
    var selected = [];

    $(".aeroleads_chk").each(function(){
      if($(this).is(":checked")){
        var curr = {};
        curr['url'] = $(this).attr("data-url");
        curr['title'] = $(this).attr("data-title");
        curr['query'] = $(this).attr("data-query");
        curr['location'] = $(this).attr("data-location");
        curr['current_company'] = $(this).attr("data-company");
        curr['current_position'] = $(this).attr("data-position");
        curr['details_str'] = $(this).attr("data-details_str");
        selected.push(curr);

        $(this).hide();
        var parent = $(this).parent();
        var aeroleads_btn = $(parent).find(".aeroleads_btn");
        $(aeroleads_btn).addClass("saved_data").html("saving...");

        $(aeroleads_btn).html("Saved");
      }
    });

    data = JSON.stringify(selected);
    var ajax_data = {
      api_key : api_key,
      data : data
    };
    var url = "http://localhost:3000/prospects";
    var url = "https://aeroleads.com/prospects";
    console.log(ajax_data);
    $.post(url, ajax_data)
    .success(function(response){
      $(".aeroleads_chk").each(function(){
        if($(this).is(":checked")){
          $(this).hide();
          var parent = $(this).parent();
          var aeroleads_btn = $(parent).find(".aeroleads_btn");
          $(aeroleads_btn).removeClass("aeroleads_btn").html("Saved");
        }
      });
    })
    .fail(function(response){
      $(".aeroleads_chk").each(function(){
        if($(this).is(":checked")){
          $(this).hide();
          var parent = $(this).parent();
          var aeroleads_btn = $(parent).find(".aeroleads_btn");
          $(aeroleads_btn).html("Error");
        }
      });
    })
    .always(function(response){
      $(".aeroleads_chk").each(function(){
        if($(this).is(":checked")){
          $(this).hide();
          var parent = $(this).parent();
          var aeroleads_btn = $(parent).find(".aeroleads_btn");
          $(aeroleads_btn).removeClass("aeroleads_btn").html("Saved");
        }
      });
    })
    console.log(selected);
  }
});

$("body").on("click", ".close_bg", function(){
  $(".api_error").hide();
});

$("body").on("click", ".close_modal", function(){
  $(".al_modal").fadeOut();
});
