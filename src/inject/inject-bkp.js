(function($){

	// GET API KEY 
	var api_key = "";
	chrome.storage.sync.get('settings',function(obj){
		if(obj.settings != null){
			$("#api_key").val(obj.settings.api_key);
		}else{
			$("#api_key").val("");
		}
		api_key =  obj.settings.api_key;
	});
	
	// GET AEROLEADS STATE
	var state = "";
	chrome.storage.sync.get('aeroleads_state',function(obj){
		if(obj.aeroleads_state != null){
			state = obj.aeroleads_state.state;
		}else{
			state = false;
		}
	});
	

	$(document).ready(function(){
		// var checkExist = setInterval(function() {
		// 	if ($("h3.r a").length) {
		// 		var list = document.querySelector('#resultStats');
		// 		var observer = new MutationObserver(function(mutations) {
		// 			mutations.forEach(function(mutation) {
		// 				if (mutation.type === 'attributes') {
		// 					checkexist();
		// 				}
		// 			});
		// 		});
		// 		observer.observe(list, {
		// 			attributes: true, 
		// 		});
		// 		getLinks();
		// 		clearInterval(checkExist);
		// 	}
		// }, 100);


		$("body").on("click", ".aeroleads_btn", function(){
			var data = {};
			var selected = [];

			var that = $(this);

			data['url'] = $(this).attr("data-url");
			data['title'] = $(this).attr("data-title");
			data['query'] = $(this).attr("data-query");
			selected.push(data);
			data = JSON.stringify(selected);

			$(that).html("saving...");

			var ajax_data = {
				api_key : api_key,
				data : data
			};
			var url = "http://localhost:3000/prospects";
			var url = "https://aeroleads.com/prospects";
			console.log(ajax_data);
			$.post(url, ajax_data)
			.success(function(response){
				$(that).addClass("saved_data").removeClass("aeroleads_btn").html("Saved");
				var parent = $(that).parent();
				var input = $(parent).find("input");
				$(input).hide();
			})
			.fail(function(response){
				$(that).addClass("saved_data").html("Error");
			})
			.always(function(response){
				$(that).addClass("saved_data").removeClass("aeroleads_btn").html("Saved");
				var parent = $(that).parent();
				var input = $(parent).find("input");
				$(input).hide();
			});
			console.log(selected);
		});

		$("body").on("click",".add_selected", function(){
			var selected = [];

			$(".aeroleads_chk").each(function(){
				if($(this).is(":checked")){
					var curr = {};
					curr['url'] = $(this).attr("data-url");
					curr['title'] = $(this).attr("data-title");
					curr['query'] = $(this).attr("data-query");
					selected.push(curr);

					$(this).hide();
					var parent = $(this).parent();
					var aeroleads_btn = $(parent).find(".aeroleads_btn");
					$(aeroleads_btn).addClass("saved_data").html("saving...");
				}
			});

			data = JSON.stringify(selected);
			var ajax_data = {
				api_key : api_key,
				data : data
			};
			var url = "http://localhost:3000/prospects";
			var url = "https://aeroleads.com/prospects";
			console.log(ajax_data);
			$.post(url, ajax_data)
			.success(function(response){
				$(".aeroleads_chk").each(function(){
					if($(this).is(":checked")){
						$(this).hide();
						var parent = $(this).parent();
						var aeroleads_btn = $(parent).find(".aeroleads_btn");
						$(aeroleads_btn).removeClass("aeroleads_btn").html("Saved");
					}
				});
			})
			.fail(function(response){
				$(".aeroleads_chk").each(function(){
					if($(this).is(":checked")){
						$(this).hide();
						var parent = $(this).parent();
						var aeroleads_btn = $(parent).find(".aeroleads_btn");
						$(aeroleads_btn).html("Error");
					}
				});
			})
			.always(function(response){
				$(".aeroleads_chk").each(function(){
					if($(this).is(":checked")){
						$(this).hide();
						var parent = $(this).parent();
						var aeroleads_btn = $(parent).find(".aeroleads_btn");
						$(aeroleads_btn).removeClass("aeroleads_btn").html("Saved");
					}
				});
			})
			console.log(selected);
		});

		$("body").on("click", "#navcnt", function(){
			window.location.reload();
			checkexist();
		});

		
		if($(".aeroleads_action").length==0 && state){
			checkexist();
		}

});

function checkexist(){
	var checkExist = setInterval(function() {
		if ($("h3.r a").length) {
			clearInterval(checkExist);
			getLinks();
		}
	}, 100);
}

function getLinks(){
	if(state)
		addTransferBtn();
}

function addTransferBtn(){
	$(".g").each(function(){
		var anchor = $(this).find("h3.r a");
		var link = $(anchor).attr("data-href");
		var query = $('title').text();
		if(typeof link == "undefined"){
			link = $(anchor).attr("href");
		}
		var title = $(anchor).text();

		var add_btn = "<div class='aeroleads_action'><input class='aeroleads_chk' type='checkbox' data-url='"+link+"' data-title='"+title+"' data-query='"+query+"'><span class='aeroleads_btn' data-url='"+link+"' data-title='"+title+"' data-query='"+query+"'>Transfer</span></div>";
		$(this).prepend(add_btn);
	});
	addSelectedBox();
}

function addSelectedBox(){
	var html = "<div class='aeroleads_add_selected'>";
	html+="<header><div class='logo'><img src='https://aeroleads.com/assets/aeroleads-icon.png' class='img-responsive'></div>";
	html+="<div class='brand'>AeroLeads</div>";
	html+="</header>";
	html+="<div class='content'><p>Transfer Selected to AeroLeads</p><span class='add_selected'>Add Selected</span></div>";
	html+="</div>";
	$("body").append(html);
}
})(jQuery);


function getAPIkey(){
	chrome.storage.sync.get('settings',function(obj){
		if(obj.settings != null){
			$("#api_key").val(obj.settings.api_key);
		}else{
			$("#api_key").val("");
		}
		console.log(obj.settings);
		return obj.settings.api_key;
	});
}

chrome.extension.sendMessage({}, function(response) {
	var readyStateCheckInterval = setInterval(function() {
		if (document.readyState === "complete") {
			clearInterval(readyStateCheckInterval);

		// ----------------------------------------------------------
		// This part of the script triggers when page is done loading
		console.log("Hello. This message was sent from scripts/inject.js");
		// ----------------------------------------------------------

	}
}, 10);
});