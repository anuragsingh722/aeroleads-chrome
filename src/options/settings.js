	var loadSettings = function(){
		chrome.storage.sync.get('aeroleads_settings',function(obj){
			if(obj.aeroleads_settings != null){
				$("#api_key").val(obj.aeroleads_settings.api_key);
			}else{
				$("#api_key").val("");
			}
			console.log(obj.aeroleads_settings);
		});
	};

	$(document).ready(function(){
		loadSettings();

		$(document).on("click", "#save-btn", function(){
			var api_key = $("#api_key").val();
			console.log(api_key);
			// chrome.storage.sync.clear(function(){});
			var user = { api_key: api_key };
			chrome.storage.sync.set({'aeroleads_settings': user}, function() {
				console.log("Saved");
				$(".alert.alert-info").css("visibility","visible");
				chrome.tabs.query({active: true, currentWindow: true}, function (arrayOfTabs) {
					var code = 'window.location.reload();';
					chrome.tabs.executeScript(arrayOfTabs[0].id, {code: code});
				});
			});
			loadSettings();
			return false;
		});

	});
