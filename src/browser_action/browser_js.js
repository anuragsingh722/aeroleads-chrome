(function($){

	var loadSettings = function(){
		chrome.storage.sync.get('aeroleads_state',function(obj){
			if(obj.aeroleads_state != null){
				var stato = true;
				stato = obj.aeroleads_state.state;
				$("#buttonThree").prop("checked", stato);
			}else{
				$("#buttonThree").prop("checked", "true");
			}
		});
	};


	$(document).ready(function(){
		loadSettings();
		$("#buttonThree").on("click", function(){
			var state = $("#buttonThree").is(":checked");
			var extn_state = { state: state };
			chrome.storage.sync.set({'aeroleads_state': extn_state}, function() {
				console.log("Saved");
				console.log(extn_state);
				chrome.tabs.query({active: true, currentWindow: true}, function (arrayOfTabs) {
					var code = 'window.location.reload();';
					chrome.tabs.executeScript(arrayOfTabs[0].id, {code: code});
				});
			});
			loadSettings();
		});
	});

})(jQuery);