(function($){

	function addDialog(){
		var html = "<div class='aeroleads_transfer'>";
		html += "<input type='hidden' class='credit_left' value='true'>";
		html += "<img src='https://aeroleads.com/assets/aeroleads-icon.png' class='img-responsive'>";
		html += "<div class='transfer_al butn-transfer'>Add Prospect to AeroLeads</div>";
		html += "</div>";
		$("body").append(html);
	}

	function addModal(){
		var html = "<div class='al_modal'>";
		html += "<div class='close_al_modal close_modal'></div>";
		html += "<div class='content_al_modal'><h3><img src='https://aeroleads.com/assets/aeroleads-icon.png' class='modal_icon'><b>Uh Oh! You do not have enough credits. </b></h3>";
		html += "<div><a href='https://aeroleads.com/pricing'>Upgrade</a> your plan or mail us at <a href='mailto:sales@aeroleads.com'>sales@aeroleads.com</a></div>"
		html += "<div style='text-align:right'><a href='#' style='cursor:pointer;color: #555; display:inline-block; margin-top: 5px;' class='close_modal'>Close</a></div>";
		html += "</div>";
		html += "</div>";
		$("body").append(html);
	}
	// GET API KEY 
	chrome.storage.sync.get('aeroleads_settings',function(obj){
		if(obj.aeroleads_settings != null){
			$("#api_key").val(obj.aeroleads_settings.api_key);
		}else{
			$("#api_key").val("");
		}
		api_key =  obj.aeroleads_settings.api_key;
	});
	
	// GET AEROLEADS STATE
	var state = "unset";
	chrome.storage.sync.get('aeroleads_state',function(obj){
		if(obj.aeroleads_state != null){
			state = obj.aeroleads_state.state;
		}else{
			state = false;
		}
	});

	function checkState(){
		var checkExist = setInterval(function() {
			console.log(state);
			if(state == false) {
				clearInterval(checkExist);
			}
			else if(state == true) {
				clearInterval(checkExist);
				addDialog();
				checkCredit();
			}
		}, 100);
	}

	function checkCredit(){
		var url = "http://localhost:3000/credit_available";
		var url = "https://aeroleads.com/credit_available";
		$.post(url, {api_key: api_key})
		.success(function(response){
			if(response == "true"){
				console.log("GOTIT");
			}
			else if(response=="false"){
				addModal();
				$(".credit_left").val("false");
			}
		})
	}

	function getUrl(){	
		var checkUrl = setInterval(function() {
			console.log(state);
			if($(".contact-public-profile a").length) {
				clearInterval(checkUrl);

				var this_url =  "";
				this_url =  $(".contact-public-profile").attr("href")
			}
		}, 100);
		return  this_url;
	}

	$(document).ready(function(){

		$("body").on("click", ".close_modal", function(){
			$(".al_modal").fadeOut();
		});

		checkState();

		$("body").on("click", ".transfer_al", function(){
			var creds = $(".credit_left").val();
			if(creds == "false"){
				$(".al_modal").fadeIn();
			}
			else if(creds == "true"){
				// get page url
				var url = $(".view-public-profile").attr("href");
				if(typeof url == "undefined"){
					$(".show-more-info").click();
					url = getUrl();
				}
				else if(url == ""){
					url = window.location.href;
				}

				var title = $("#name .full-name").text();
				var query = title;
				if(typeof title == "undefined"){
					title = "NA";
					query = title;
				}
				var current_company = $(".current-position header h5 span strong a");
				if(current_company.length){
					current_company = $(current_company).first().text();
				}
				else current_company = "NA";

				var current_position = $(".current-position header h4 a");
				if(current_position.length){
					current_position = $(current_position).first().text();
				}
				else current_position = "NA";

				var current_location = $("#location-container #location .locality a");
				if(current_location.length){
					current_location = $(current_location).first().text();
				}
				else current_location = "NA";

				var that = $(this);
				var selected = [];
				var data = {};

				data['url'] = url;
				data['title'] = title;
				data['query'] = query;
				data['current_company'] = current_company;
				data['current_position'] = current_position;
				data['location'] = current_location;

				selected.push(data);
				data = JSON.stringify(selected);

				var ajax_data = {
					api_key : api_key,
					data : data
				};

				var url = "http://localhost:3000/prospects";
				var url = "https://aeroleads.com/prospects";
				console.log(ajax_data);
				$.post(url, ajax_data)
				.success(function(response){
					$(that).addClass("saved_data").removeClass("transfer_al").html("Saved");
					var parent = $(that).parent();
					var input = $(parent).find("input");
					$(input).hide();
				})
				.fail(function(response){
					$(that).addClass("saved_data").html("Error");
				})
				.always(function(response){
					$(that).addClass("saved_data").removeClass("transfer_al").html("Saved");
					var parent = $(that).parent();
					var input = $(parent).find("input");
					$(input).hide();
				});
			}
		});
});

})(jQuery);